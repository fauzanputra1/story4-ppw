from django.forms import ModelForm
from django import forms
from .models import Course
from django.core.validators import MaxValueValidator

GENAP_20 = 'GN 19/20'
GASAL_20 = 'GA 19/20'
GENAP_19 = 'GN 18/19'
GASAL_19 = 'GA 18/19'
SEMESTER = [
    (GENAP_20, 'Genap 2019/2020'),
    (GASAL_20, 'Gasal 2019/2020'),
    (GENAP_19, 'Genap 2018/2019'),
    (GASAL_19, 'Gasal 2018/2019')
]

class CreateCourse(ModelForm):
    class Meta:
        model = Course
        fields = ['course_name', 'teaching_professor', 
            'credit_amount', 'semester', 'classroom', 'description']

        error_messages = {
            'max_length' : 'Your Input is too long',
            'required' : 'Input is required'
        }
        
        input_attrs = {
            'type' : 'text',
        }

    course_name = forms.CharField(max_length=30, required=True)
    teaching_professor = forms.CharField(max_length=40)
    credit_amount = forms.IntegerField(validators=[MaxValueValidator(10)])
    description = forms.CharField(max_length=150)
    semester = forms.Select(choices=SEMESTER)
    classroom = forms.CharField(max_length=20)