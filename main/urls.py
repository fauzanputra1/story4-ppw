from django.urls import path, re_path
from . import views #savecourse


app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('experience', views.experience, name="experience"),
    path('project', views.project, name="project"),
    path('contact', views.contact, name="contact"),
    path('video', views.video, name="video"),
    path('course', views.course, name="course" ),
    path('savecourse', views.savecourse, name="savecourse"),
    path('createcourse', views.createcourse, name="createcourse")
]
