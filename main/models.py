from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.core.validators import MaxValueValidator

class Course(models.Model):
    GENAP_20 = 'GN 19/20'
    GASAL_20 = 'GA 19/20'
    GENAP_19 = 'GN 18/19'
    GASAL_19 = 'GA 18/19'
    SEMESTER = [
        (GENAP_20, 'Genap 2019/2020'),
        (GASAL_20, 'Gasal 2019/2020'),
        (GENAP_19, 'Genap 2018/2019'),
        (GASAL_19, 'Gasal 2018/2019')
    ]

    course_name = models.CharField(max_length=30)
    teaching_professor = models.CharField(max_length=40, default='')
    credit_amount = models.IntegerField(validators=[MaxValueValidator(10)], default='0')
    description = models.CharField(max_length=150, default='')
    semester = models.CharField(max_length=20, 
        choices=SEMESTER,
        default=GENAP_20)
    classroom = models.CharField(max_length=20, default='')