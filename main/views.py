from django.shortcuts import render
from .models import Course
from django.http import HttpResponseRedirect
from .forms import CreateCourse

def home(request):
    return render(request, 'main/home.html')

def experience(request):
    return render(request, 'main/experience.html')

def project(request):
    return render(request, 'main/project.html')

def contact(request):
    return render(request, 'main/contact.html')

def video(request):
    return render(request, 'main/video.html')

def course(request):
    stuff = Course.objects.all()
    cours = { 'courses_data' : stuff} 

    return render(request, 'main/course.html', cours)

def createcourse(request):
    context = {}
    context['form'] = CreateCourse()

    return render(request, 'main/createcourse.html', context)

def savecourse(request):
    form = CreateCourse(request.POST or None)
    if(form.is_valid and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/course')
    else:
        return HttpResponseRedirect('/createcourse')